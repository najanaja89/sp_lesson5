﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace sp_lesson5
{
    class Program
    {
        static void Main(string[] args)
        {
            var threads = new Thread[20];
            var counter = new Counter();
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(counter.IncreaseNumber);
            }

            foreach (var thread in threads)
            {
                thread.Start();
            }
            Console.ReadLine();
        }
    }
}
