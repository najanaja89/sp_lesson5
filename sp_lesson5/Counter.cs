﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace sp_lesson5
{
    [Synchronization]
    public class Counter : ContextBoundObject
    {
        private int number = -1;
        private object lockObject = new object();

        public void IncreaseNumber()
        {

            //потоки работают с одной переменной
            //lock(lockObject)
            //{
            //    while (number <= 10)
            //    {
            //        Console.WriteLine($"Thread Started to work: {Thread.CurrentThread.ManagedThreadId}");
            //        Console.Write(number + " ");
            //        number++;
            //        Thread.Sleep(5 * new Random().Next(100));
            //    }
            //}


            //Работа блокировка метода для одного потока
            //Monitor.Enter(lockObject);

            //try
            //{
            //    while (number <= 10)
            //    {
            //        Console.WriteLine($"Thread Started to work: {Thread.CurrentThread.ManagedThreadId}");
            //        Console.Write(number + " ");
            //        number++;
            //        Thread.Sleep(5 * new Random().Next(100));
            //    }
            //}
            //finally
            //{
            //    Monitor.Exit(lockObject);
            //}


            //весь объект блокируется для одного потока
            while (number <= 10)
            {
                Console.WriteLine($"Thread Started to work: {Thread.CurrentThread.ManagedThreadId}");
                Console.Write(number + " ");
                number++;
                Thread.Sleep(5 * new Random().Next(100));
            }

        }
    }
}
